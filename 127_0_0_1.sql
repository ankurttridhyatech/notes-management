-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 12, 2020 at 08:10 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `notesmanagement`
--
CREATE DATABASE IF NOT EXISTS `notesmanagement` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `notesmanagement`;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(3, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(4, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(5, '2016_06_01_000004_create_oauth_clients_table', 1),
(6, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(7, '2019_08_19_000000_create_failed_jobs_table', 1),
(10, '2020_06_10_072741_create_notes_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `notes`
--

CREATE TABLE `notes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discription` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notes`
--

INSERT INTO `notes` (`id`, `user_id`, `discription`, `color`, `created_at`, `updated_at`) VALUES
(5, '5', 'work', 'sky blue', '2020-06-11 14:23:48', '2020-06-12 13:04:45'),
(10, '5', 'testing', 'light green', '2020-06-11 09:36:06', '2020-06-11 09:36:06'),
(11, '5', 'notes are not saved', 'sky blue', '2020-06-11 09:36:33', '2020-06-11 09:36:33'),
(12, '5', 'helo', 'red', '2020-06-11 09:36:59', '2020-06-11 09:36:59'),
(13, '5', 'notes saved', 'red', '2020-06-11 09:37:35', '2020-06-11 09:37:35'),
(14, '8', 'works', 'red', '2020-06-12 00:16:26', '2020-06-12 00:16:26');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('109ecda7738fce1434358180d23629a03ce11020c63894d87231005034677432ffef1cc4ac76d63f', 3, 1, 'MyApp', '[]', 0, '2020-06-10 07:17:29', '2020-06-10 07:17:29', '2021-06-10 12:47:29'),
('1f92a0c12f56a8a9855619aa1fe5d01fa7e2600ba257b462a9c446c6a7759e3ea895e940ca699ddb', 8, 1, 'MyApp', '[]', 1, '2020-06-12 00:15:37', '2020-06-12 00:15:37', '2021-06-12 05:45:37'),
('28928fdd1a0fac7a02ec356b7e7940c87b0bbec1f8a16044e92826ebebce106fe32c27f9040120b9', 5, 1, 'MyApp', '[]', 1, '2020-06-11 08:37:15', '2020-06-11 08:37:15', '2021-06-11 14:07:15'),
('3b7dd3e4dfbad4a291f6f3d26afbd5eb5e1fb9796d8841f4395254bfb0e5dc8b4b6859ae29013cf0', 8, 1, 'MyApp', '[]', 0, '2020-06-12 00:14:55', '2020-06-12 00:14:55', '2021-06-12 05:44:55'),
('40162ff041351f78871037e2c3863fdb40442b42b1f5673520c184126916a2f277a55d65201c8090', 2, 1, 'MyApp', '[]', 0, '2020-06-09 23:22:20', '2020-06-09 23:22:20', '2021-06-10 04:52:20'),
('4172d5cce880328df114bb2d08ebd886cdaac6afa68cc5c04b43439999de3b2ef391092ef87c1ae8', 4, 1, 'MyApp', '[]', 0, '2020-06-11 01:53:18', '2020-06-11 01:53:18', '2021-06-11 07:23:18'),
('4ac5f757cd2bd9c1cfbb026abb79761987ced55de075bde5a9edb1edf325e3fe6263a7136ed3e43a', 2, 1, 'MyApp', '[]', 0, '2020-06-10 07:18:13', '2020-06-10 07:18:13', '2021-06-10 12:48:13'),
('4fc26e6d6a33f6efa5c4d96c0efed5051f9fb35bed8d7a94d7d936ade213de4a880a94b4ac31623c', 1, 1, 'MyApp', '[]', 1, '2020-06-09 09:35:03', '2020-06-09 09:35:03', '2021-06-09 15:05:03'),
('71164f37340a8f1581aa6ce852179cdab7198dbe8855eef32569c0b6157d8ad397989c74777129dc', 4, 1, 'MyApp', '[]', 1, '2020-06-11 01:54:56', '2020-06-11 01:54:56', '2021-06-11 07:24:56'),
('803617854d6b0a43781eb55025646a5287bed3acc6a8d63359bdc33657fb49a1414569aabfae7a57', 1, 1, 'MyApp', '[]', 0, '2020-06-09 23:22:50', '2020-06-09 23:22:50', '2021-06-10 04:52:50'),
('a32e94dc12e3397c9699dd9c4692a941dec2f86a746c3081af2b971a95b6fb65f1636e9200595917', 5, 1, 'MyApp', '[]', 0, '2020-06-11 08:25:04', '2020-06-11 08:25:04', '2021-06-11 13:55:04'),
('b4f9b3ff232edbde924bb8ab51865b410e4ff7d766734e10d422e966923f878f19ff980bef373590', 1, 1, 'MyApp', '[]', 0, '2020-06-09 10:39:49', '2020-06-09 10:39:49', '2021-06-09 16:09:49'),
('bf97aeea2271a614468baaf2bd264b7f03c2606dc42f40b60cea6e855d4a86042d4db7880e31596d', 2, 1, 'MyApp', '[]', 1, '2020-06-09 23:23:23', '2020-06-09 23:23:23', '2021-06-10 04:53:23'),
('cede69df198b14e6688856a08b7d9e381d9a4fd4f873965b60369ef7ac6b99090164bed5383291ce', 2, 1, 'MyApp', '[]', 1, '2020-06-09 23:27:37', '2020-06-09 23:27:37', '2021-06-10 04:57:37'),
('d63f23f3bba6a5a29f9ebdc0c553cfa1e55dcb7c4284c011c158825a675280f5df34814f6256c157', 2, 1, 'MyApp', '[]', 0, '2020-06-10 01:07:07', '2020-06-10 01:07:07', '2021-06-10 06:37:07'),
('f3b6603dec5d5c56ced59bcf4e7bdec05d38dd7297d70341d29e2ee085295b31992294c722b5f935', 2, 1, 'MyApp', '[]', 1, '2020-06-10 02:48:09', '2020-06-10 02:48:09', '2021-06-10 08:18:09');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'nOEAWXSw8cFrN8G7w8sLvEDEf0g7iVmzE7blfM9D', NULL, 'http://localhost', 1, 0, 0, '2020-06-09 08:04:40', '2020-06-09 08:04:40'),
(2, NULL, 'Laravel Password Grant Client', '51aczJlgv18JgMd7vbEE4xbh6xLEj1GvMuOG4KHM', 'users', 'http://localhost', 0, 1, 0, '2020-06-09 08:04:41', '2020-06-09 08:04:41');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-06-09 08:04:41', '2020-06-09 08:04:41');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(5, 'abc', 'abc@gmail.com', NULL, '$2y$10$hIvEHn3HnjtrnDzt4/RtM.AV2qgSKYPNmZkKrT2HhUAhj4YCBwz8O', NULL, '2020-06-11 08:25:04', '2020-06-11 08:25:04'),
(8, 'abc1', 'abc1@gmail.com', NULL, '$2y$10$V36IcaLRUdJf599PPZM0JOzXgOjCkgeslgWUUXY8Kwv1mrU8nFYO.', NULL, '2020-06-12 00:14:54', '2020-06-12 00:14:54');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notes`
--
ALTER TABLE `notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `notes`
--
ALTER TABLE `notes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
