<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['prefix' => 'auth'], function () {

Route::post('login', 'API\UserController@login')->name('login');
Route::post('register', 'API\UserController@register')->name('register');

Route::group(['middleware' => 'auth:api'], function(){
Route::post('addNote','API\NoteController@addNote')->name('addNote');
Route::delete('deleteNote/{id}','API\NoteController@deleteNote')->name('deleteNote');
Route::put('editNote/{id}','API\NoteController@editNote')->name('editNote');
Route::get('allNote','API\NoteController@allNote')->name('allNote');
Route::get('colorNote','API\NoteController@colorNote')->name('colorNote');
Route::post('logout','API\UserController@logout')->name('logout');

});

});





