<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use Illuminate\Support\Facades\Log;

class UserController extends Controller 
{
    public $successStatus = 200;
    /**     
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $user = Auth::user(); 
            $success['token'] =  $user->createToken('MyApp')-> accessToken; 
            $data[] = $success;
            $success = true;
            $message = "Successfully Login";
            Log::info($message);
            return $this->response($success,$message,$data); 
        } 
        else{ 
            $data[] = [];          
            $success = false;
            $message = "Unauthorised";
            Log::error($message);
            return $this->response($success,$message,$data);      
        } 
    }

    /** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function register(Request $request) 
    { 
        $validator = Validator::make($request->all(), [ 
            'name' => 'required', 
            'email' => 'required|email|unique:users', 
            'password' => 'required', 
            'c_password' => 'required|same:password', 
        ]);
         if ($validator->fails()) { 
               $data[] = [];          
               $success = false;
               $message = $validator->errors();
               Log::error($message);
               return $this->response($success,$message,$data);             
        }
        
        $input = $request->all(); 
        $input['password'] = bcrypt($input['password']); 
        $user = User::create($input); 
        $success['name'] =  $user->name;
        $data[] = $success;
        $success = true;
        $message = "Successfully Registered";
        Log::info($message);
        return $this->response($success,$message,$data); 
    }
    /** 
     * Logout api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function logout(){   
        if (Auth::check()) {
            Auth::user()->token()->revoke();
            $data[] = [];
            $success = true;
            $message = "Successfully logout";
            Log::info($message);
            return $this->response($success,$message,$data); 
        }
        else{
            $data[] = [];          
            $success = false;
            $message = "Something went wrong";
            Log::error($message);
            return $this->response($success,$message,$data);    
            }
        }

    public function response($success,$message,$data)
    {
        return response()->json([
        'success' => $success,
        'message' => $message,
        'data' => $data
        ]);
    }
}