<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\User; 
use Illuminate\Support\Facades\Auth; 
use Validator;
use App\Note;
use Illuminate\Support\Facades\Log;

class NoteController extends Controller
{
    //

    public function allNote()
    {  
    	$note = Note::all();
    	if($note->isEmpty()){
            $data[] = [];          
            $success = false;
            $message =  "Record not found";
            Log::error($message);
            return $this->response($success,$message,$data);  
          }
        else{
          $data[] = $note;
          $success = true;
          $message = "List of all notes";
          Log::info($message);
          return $this->response($success,$message,$data); 
  		}
    } 

    public function colorNote(Request $request)
    { 
      $validator = Validator::make($request->all(), [ 
             'search' => 'in:sky blue,lavonder,red,light green,yellow,dark green',
           ]);
      if ($validator->fails()) { 
                $data[] = [];          
                $success = false;
                $message = $validator->errors();
                Log::error($message);
                return $this->response($success,$message,$data);                 
            }
    	elseif ($request->has('search') && $request != '') {
          $note = Note::where('color','=',$request->search)->get();
         		if($note->isEmpty())
          		{
                $data[] = [];          
                $success = false;
                $message =  "No notes available";
                Log::error($message);
                return $this->response($success,$message,$data);  
          		}
          		else{
                   $data[] = $note;
                   $success = true;
                   $message = "Your color notes";
                   Log::info($message);
                   return $this->response($success,$message,$data);
          		}	
        }
        else{
            $data[] = [];          
            $success = false;
            $message =  "Record not found";
            Log::error($message);
            return $this->response($success,$message,$data);  
        }
    } 

    public function addNote(Request $request)
    {
    	$validator = Validator::make($request->all(), [ 
            'discription' => 'required', 
            'color' => 'required|in:sky blue,lavonder,red,light green,yellow,dark green'  ,
        ]);
        if ($validator->fails()) { 
             $data[] = [];          
             $success = false;
             $message = $validator->errors();
             Log::error($message);
             return $this->response($success,$message,$data);      
        }
        else{
            $note = new Note();
            $note->user_id = auth('api')->user()->id;
            $note->discription = $request->discription;
            $note->color = $request->color;
            $note->save();
            $data[] = $note;
            $success = true;
            $message = "Add note successfully";
            Log::info($message);
            return $this->response($success,$message,$data); 
        }
    }

    public function deleteNote(Request $request,$id)
    {
    	$note= Note::find($id);
        $sucess = "Delete Successfully";
        if($note== null){
            $data[] = [];          
            $success = false;
            $message = "Record not found!";
            Log::error($message);
            return $this->response($success,$message,$data); 
        }
        else{
            $note->delete();
            $data[] = [];
            $success = true;
            $message = "Delete note successfully";
            Log::info($message);
            return $this->response($success,$message,$data); 	
        }
    }

	public function editNote(Request $request,$id)
    {
        $note = note::find($id);
        if($note == null){
            $data[] = [];          
            $success = false;
            $message = "Record not found!";
            Log::error($message); 
            return $this->response($success,$message,$data);     
        }
        $validator = Validator::make($request->all(), [ 
            'discription' => 'required', 
             'color' => 'required|in:sky blue,lavonder,red,light green,yellow,dark green'  ,
           ]);
        if ($validator->fails()) { 
                $data[] = [];          
                $success = false;
                $message = $validator->errors();
                Log::error($message);
                return $this->response($success,$message,$data);                 
            }
        else{
              $note->discription = $request->discription;
              $note->color = $request->color;
              $note->save();
              $data[] = $note;
              $success = true;
              $message = "Edit note successfully";
              Log::info($message); 
              return $this->response($success,$message,$data); 
        }
    }

   public function response($success,$message,$data)
    {
        return response()->json([
        'success' => $success,
        'message' => $message,
        'data' => $data
        ]);
    }
}
